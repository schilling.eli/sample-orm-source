resource "oci_core_instance" "webserver1" {
  availability_domain = lookup(data.oci_identity_availability_domains.availability_domains.availability_domains[var.availability_domains -2],"name")
  compartment_id = var.compartment_ocid
  display_name = "Webserver 01"
  shape = var.instance_shape

  create_vnic_details {
    subnet_id = oci_core_subnet.lb-backendset1-subnet.id
    hostname_label = "orm-demo-ws1"
    display_name = "Webserver1"
    assign_public_ip = "false"
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(file(var.bootstrap_file))
  }

  source_details {
    source_type = "image"
    source_id = data.oci_core_images.OL7ImageOCID.images[0]["id"]
  }
}

resource "oci_core_instance" "webserver2" {
  availability_domain = lookup(data.oci_identity_availability_domains.availability_domains.availability_domains[var.availability_domains -1],"name")
  compartment_id = var.compartment_ocid
  shape = var.instance_shape
  display_name = "Webserver 2"

  create_vnic_details {
    subnet_id = oci_core_subnet.lb-backendset2-subnet.id
    hostname_label = "orm-demo-ws2"
    display_name = "Webserver2"
    assign_public_ip = "false"
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(file(var.bootstrap_file))
    assign_public_ip = "false"
  }

  source_details {
    source_type = "image"
    source_id = data.oci_core_images.OL7ImageOCID.images[0]["id"]
  }
}

resource "oci_core_instance" "webserver3" {
  availability_domain = lookup(data.oci_identity_availability_domains.availability_domains.availability_domains[var.availability_domains -2],"name")
  compartment_id = var.compartment_ocid
  display_name = "Webserver 03"
  shape = var.instance_shape

  create_vnic_details {
    subnet_id = oci_core_subnet.lb-backendset1-subnet.id
    hostname_label = "orm-demo-ws3"
    display_name = "Webserver3"
    assign_public_ip = "false"
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(file(var.bootstrap_file))
  }

  source_details {
    source_type = "image"
    source_id = data.oci_core_images.OL7ImageOCID.images[0]["id"]
  }
}
